from Brownian import Brownian
from Brownian import COLORS
from Brownian import create_potential

import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from numba import jit


@jit(nopython=True)
def infect(x, y, s, threshold=0.1, probability=0.2):
    snew = np.copy(s)
    for i in range(s.size):
        if s[i] == 1:
            for j in range(s.size):
                if s[j] == 0:
                    if (x[i]-x[j])**2 + (y[i]-y[j])**2 <= threshold**2:
                        if np.random.rand() <= probability:
                            snew[j] = 1
    return snew

class SI(Brownian):

    """Docstring for SI. """

    def __init__(self, infected=1, distance=0.1, likelihood=0.5, **kwargs):
        """TODO: to be defined. """
        Brownian.__init__(self, **kwargs)
        assert infected <= self.population
        self.infected = [infected]
        self.healthy = [self.population - infected]
        for i in range(infected):
            self.straj[0][i] = 1
        np.random.shuffle(self.straj[0])
        self.distance = distance
        self.likelihood = likelihood

    def run(self):
        Brownian.run(self)
        for i in range(1, self.steps):
            print(f"Infecting... step: {i:5}")
            self.straj[i] = infect(self.xtraj[i-1], self.ytraj[i-1], self.straj[i-1])
            self.ctraj[i] = [COLORS[j] for j in self.straj[i]]
            self.healthy.append(np.count_nonzero(self.straj[i]==0))
            self.infected.append(np.count_nonzero(self.straj[i]==1))

    def graph(self):

        fig, [ax1, ax2] = plt.subplots(2,1)
        xdata, ydata = [], []
        ln1, = ax1.plot([], [], c=COLORS[0])
        ln2, = ax1.plot([], [], c=COLORS[1])
        scat = plt.scatter([], [])


        def init():
            ax1.set_xlim(0, self.steps)
            ax1.set_ylim(-1, self.population+1)
            ax2.set_xlim(0, self.box_size[0])
            ax2.set_ylim(0, self.box_size[1])
            ax2.set_aspect("equal")
            return ln1, ln2, scat


        def update(frame):
            ln1.set_data(np.arange(frame), self.healthy[:frame])
            ln2.set_data(np.arange(frame), self.infected[:frame])
            xdata = self.xtraj[frame]
            ydata = self.ytraj[frame]
            data = np.vstack((xdata, ydata)).T
            scat.set_offsets(data)
            scat.set_color(self.ctraj[frame])
            return ln1, ln2, scat

        ani = FuncAnimation(fig, update, frames=np.arange(0, self.steps),
                            init_func=init, blit=True, interval=1000/self.steps)
        plt.show()


def main():
    si = SI(population=100, infected=10, box_size=(2,2), steps=2000, likelihood=0.5, distance=0.1, seed=123)
    # si.set_potential(create_potential(k=1))
    si.run()
    si.graph()

if __name__ == "__main__":
    main()
