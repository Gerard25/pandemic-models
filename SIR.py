from Brownian import Brownian
from Brownian import COLORS
from Brownian import create_potential

import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from numba import jit


@jit(nopython=True)
def infect(x, y, s, threshold=0.1, probability=0.2):
    snew = np.copy(s)
    for i in range(s.size):
        if s[i] == 1:
            for j in range(s.size):
                if s[j] == 0:
                    if (x[i]-x[j])**2 + (y[i]-y[j])**2 <= threshold**2:
                        if np.random.rand() <= probability:
                            snew[j] = 1
    return snew

@jit(nopython=True)
def restore(sold, s):
    snew = np.copy(s)
    for i in range(s.size):
        if sold[i] == 1 and s[i] == 1:
            snew[i] = 2
    return snew

class SIR(Brownian):

    """Docstring for SIR. """

    def __init__(self, infected=1, distance=0.1, likelihood=0.2, recovering_time=100, **kwargs):
        """TODO: to be defined. """
        Brownian.__init__(self, **kwargs)
        assert infected <= self.population
        self.infected = [infected]
        self.healthy = [self.population - infected]
        self.recovered = [0]
        for i in range(infected):
            self.straj[0][i] = 1
        np.random.shuffle(self.straj[0])
        self.distance = distance
        self.likelihood = likelihood
        assert recovering_time < self.steps
        self.recovering_time = recovering_time

    def run(self):
        Brownian.run(self)
        for i in range(1, self.steps):
            print(f"Infecting... step: {i:5}")
            if i <= self.recovering_time:
                self.straj[i] = infect(self.xtraj[i-1], self.ytraj[i-1], self.straj[i-1])
            else:
                self.straj[i] = restore(self.straj[i-1-self.recovering_time], infect(self.xtraj[i-1], self.ytraj[i-1], self.straj[i-1]))
            self.ctraj[i] = [COLORS[j] for j in self.straj[i]]
            self.healthy.append(np.count_nonzero(self.straj[i]==0))
            self.infected.append(np.count_nonzero(self.straj[i]==1))
            self.recovered.append(np.count_nonzero(self.straj[i]==2))

    def graph(self):

        fig, [ax1, ax2] = plt.subplots(2,1)
        xdata, ydata = [], []
        ln1, = ax1.plot([], [], c=COLORS[0])
        ln2, = ax1.plot([], [], c=COLORS[1])
        ln3, = ax1.plot([], [], c=COLORS[2])
        scat = plt.scatter([], [])


        def init():
            ax1.set_xlim(0, self.steps)
            ax1.set_ylim(-1, self.population+1)
            ax2.set_xlim(0, self.box_size[0])
            ax2.set_ylim(0, self.box_size[1])
            ax2.set_aspect("equal")
            return ln1, ln2, scat


        def update(frame):
            ln1.set_data(np.arange(frame), self.healthy[:frame])
            ln2.set_data(np.arange(frame), self.infected[:frame])
            ln3.set_data(np.arange(frame), self.recovered[:frame])
            xdata = self.xtraj[frame]
            ydata = self.ytraj[frame]
            data = np.vstack((xdata, ydata)).T
            scat.set_offsets(data)
            scat.set_color(self.ctraj[frame])
            return ln1, ln2, ln3, scat

        ani = FuncAnimation(fig, update, frames=np.arange(0, self.steps),
                            init_func=init, blit=True, interval=1000/self.steps)
        plt.show()


def main():
    sir = SIR(population=800, box_size=(5,5), steps=2000, recovering_time=300, likelihood=1, distance=0.5, seed=123)
    # sir.set_potential(create_potential())
    sir.run()
    sir.graph()

if __name__ == "__main__":
    main()
