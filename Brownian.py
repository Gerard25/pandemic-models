import random
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from numba import jit


COLORS = {
    0: "#008000",
    1: "#800000",
    2: "#000080"
}

@jit(nopython=True)
def grad(V, xdata, ydata, delta=1e-4):
    dimnumber = xdata.size
    gradx = np.zeros(xdata.size)
    grady = np.zeros(ydata.size)
    V0 = V(xdata, ydata)
    for i in range(dimnumber):
        tmp = np.copy(xdata)
        tmp[i] += delta
        gradx[i] = (V(tmp, ydata) - V0) / delta
    for i in range(dimnumber):
        tmp = np.copy(ydata)
        tmp[i] += delta
        grady[i] = (V(xdata, tmp) - V0) / delta
    return gradx, grady

def create_potential(n=1, k=1):
    @jit(nopython=True)
    def repulsion(xdata, ydata):
        V = 0
        for i in range(xdata.size):
            for j in range(i+1, xdata.size):
                V += k/np.sqrt(((xdata[i]-xdata[j])**2 + (ydata[i]-ydata[j])**2))**n
        return V
    return repulsion

class Brownian(object):

    """Docstring for Brownian. """

    def __init__(self, population=100, box_size=(1,1), steps=1000, seed=0):
        """TODO: to be defined. """
        self.population = population
        self.box_size = box_size
        self.steps = steps
        random.seed(seed)
        np.random.seed(seed)
        self.xtraj = [self.box_size[0] * np.random.uniform(size=self.population)]
        self.ytraj = [self.box_size[1] * np.random.uniform(size=self.population)]
        self.straj = [np.zeros(population)]
        self.ctraj = [COLORS[j] for j in self.straj[0]]
        self.potential = None

    def set_potential(self, pot):
        self.potential = pot

    @staticmethod
    def __step(xdata, ydata, dt=1, V=None, bounds=[0,1,0,1]):
        xnew, ynew = np.empty(xdata.shape, dtype=np.float_), np.empty(ydata.shape, dtype=np.float_)
        if V is None:
            gradx = np.zeros(xdata.shape)
            grady = np.zeros(ydata.shape)
        else:
            gradx, grady = grad(V, xdata, ydata)

        for i in range(xdata.size):
            if abs(dt * gradx[i]) > abs(bounds[1] - bounds[0]) or abs(dt * grady[i]) > abs(bounds[3] - bounds[2]):
                print("Huge value of gradient, probably simulation is numerically unstable!\nTry to decrease n or k parameter of potential!")
            x = xdata[i] + np.sqrt(dt) * random.gauss(0, 1) - dt * gradx[i]
            y = ydata[i] + np.sqrt(dt) * random.gauss(0, 1) - dt * grady[i]
            if x < bounds[0]:
                xnew[i] = bounds[0] + abs(bounds[0] - x)
            elif x > bounds[1]:
                xnew[i] = bounds[1] - abs(bounds[1] - x)
            else:
                xnew[i] = x
            if y < bounds[2]:
                ynew[i] = bounds[2] + abs(bounds[2] - y)
            elif y > bounds[3]:
                ynew[i] = bounds[3] - abs(bounds[3] - y)
            else:
                ynew[i] = y
        return xnew, ynew

    def run(self):
        for i in range(1, self.steps):
            print(f"Step: {i:5}")
            x, y = self.__step(
                        self.xtraj[i-1],
                        self.ytraj[i-1],
                        dt=1e-4,
                        V=self.potential,
                        bounds=[0, self.box_size[0], 0, self.box_size[1]]
                        )
            self.xtraj.append(x)
            self.ytraj.append(y)
            self.straj.append(self.straj[0])
            self.ctraj.append(self.ctraj[0])

    def visualize(self):
        fig, ax1 = plt.subplots(1,1)
        xdata, ydata = [], []
        scat = plt.scatter([], [])

        def init():
            ax1.set_xlim(0, self.box_size[0])
            ax1.set_ylim(0, self.box_size[1])
            ax1.set_aspect("equal")
            return scat,

        def update(frame):
            xdata = self.xtraj[frame]
            ydata = self.ytraj[frame]
            data = np.vstack((xdata, ydata)).T
            scat.set_offsets(data)
            scat.set_color(self.ctraj[frame])
            return scat,


        ani = FuncAnimation(fig, update, frames=np.arange(0, self.steps),
                            init_func=init, blit=True, interval=10)
        plt.show()


def main():
    b = Brownian(population=100, box_size=(3,3), steps=1000)
    b.set_potential(create_potential(n=1, k=1))
    b.run()
    b.visualize()

if __name__ == "__main__":
    main()

